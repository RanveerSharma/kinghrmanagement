﻿using System;
using System.Collections.Generic;

namespace Client.Domain.Entities.HRMANAGEMENT;

public partial class EmployeeResignation
{
    public int Id { get; set; }

    public string EmpCode { get; set; } = null!;

    public string SubmittedByEmpCode { get; set; } = null!;

    public int UnitCode { get; set; }

    public DateTime DateOfResignation { get; set; }

    public DateTime DateOfReleaving { get; set; }

    public DateTime UpdatedDateOfReleaving { get; set; }

    public string? DateOfReleavingUpdatedBy { get; set; }

    public string ResignationReason { get; set; } = null!;

    public DateTime CreatedAt { get; set; }

    public bool IsActive { get; set; }

    public virtual ICollection<ResignationNocdetail> ResignationNocdetails { get; set; } = new List<ResignationNocdetail>();
}

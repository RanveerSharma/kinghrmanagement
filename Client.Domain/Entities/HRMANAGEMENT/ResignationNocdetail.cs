﻿using System;
using System.Collections.Generic;

namespace Client.Domain.Entities.HRMANAGEMENT;

public partial class ResignationNocdetail
{
    public int Id { get; set; }

    public int EmployeeResignationId { get; set; }

    public int DepartmentId { get; set; }

    public DateTime? NocDate { get; set; }

    public string? NocDocument { get; set; }

    public string? NocBy { get; set; }

    public DateTime CreatedAt { get; set; }

    public bool IsActive { get; set; }

    public virtual EmployeeResignation EmployeeResignation { get; set; } = null!;
}

﻿using Abp.Domain.Uow;

namespace Client.Domain.Repositories
{
    public interface IRepository
    {
        IUnitOfWork UnitOfWork { get; }
    }
}

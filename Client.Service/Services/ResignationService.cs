﻿using Client.Domain.Repositories;
using Client.Service.Contract;
using Client.Service.Mapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Client.Service.Services
{
    public class ResignationService: IResignationService
    {
        private readonly IResignationMapper _IResignationMapper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHRManagementRepository _HRManagementRepository;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        public ResignationService(IServiceScopeFactory serviceScopeFactory, IHRManagementRepository hrManagementRepository, IResignationMapper resignationMapper, IHttpContextAccessor httpContextAccessor)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _HRManagementRepository = hrManagementRepository ?? throw new ArgumentNullException(nameof(hrManagementRepository));
            _httpContextAccessor = httpContextAccessor;
            _IResignationMapper = resignationMapper ?? throw new ArgumentNullException(nameof(resignationMapper));
        }

    }
}

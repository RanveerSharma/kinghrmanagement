﻿using Client.Domain.Repositories;
using Client.Repository.Database.HRMANAGEMENT;

namespace Client.Repository.Repositories
{
    public class HRManagementRepository : IHRManagementRepository
    {
        private readonly KinghrmanagementContext _dbContext;
        public IUnitOfWork UnitOfWork => _dbContext;
        public HRManagementRepository(KinghrmanagementContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }
    }
}

﻿using Client.Domain.Entities.HRMANAGEMENT;
using Microsoft.EntityFrameworkCore;

namespace Client.Repository.Database.HRMANAGEMENT;

public partial class KinghrmanagementContext : DbContext
{
    public KinghrmanagementContext()
    {
    }

    public KinghrmanagementContext(DbContextOptions<KinghrmanagementContext> options)
        : base(options)
    {
    }

    public virtual DbSet<EmployeeResignation> EmployeeResignations { get; set; }

    public virtual DbSet<ResignationNocdetail> ResignationNocdetails { get; set; }

//    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
//        => optionsBuilder.UseSqlServer("Server=192.168.18.55,1434;Database=KINGHRMANAGEMENT;User ID=sa;Password=Welcome123;TrustServerCertificate=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<EmployeeResignation>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Employee__3214EC2712157FED");

            entity.ToTable("EmployeeResignation");

            entity.Property(e => e.Id).HasColumnName("ID");
            entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            entity.Property(e => e.DateOfReleaving).HasColumnType("datetime");
            entity.Property(e => e.DateOfReleavingUpdatedBy)
                .HasMaxLength(55)
                .IsUnicode(false);
            entity.Property(e => e.DateOfResignation).HasColumnType("datetime");
            entity.Property(e => e.EmpCode)
                .HasMaxLength(55)
                .IsUnicode(false);
            entity.Property(e => e.ResignationReason).IsUnicode(false);
            entity.Property(e => e.SubmittedByEmpCode)
                .HasMaxLength(55)
                .IsUnicode(false);
            entity.Property(e => e.UpdatedDateOfReleaving).HasColumnType("datetime");
        });

        modelBuilder.Entity<ResignationNocdetail>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Resignat__3214EC27CDF65EE0");

            entity.ToTable("ResignationNOCDetail");

            entity.Property(e => e.Id).HasColumnName("ID");
            entity.Property(e => e.CreatedAt).HasColumnType("datetime");
            entity.Property(e => e.NocBy)
                .HasMaxLength(55)
                .IsUnicode(false);
            entity.Property(e => e.NocDate).HasColumnType("datetime");
            entity.Property(e => e.NocDocument).IsUnicode(false);

            entity.HasOne(d => d.EmployeeResignation).WithMany(p => p.ResignationNocdetails)
                .HasForeignKey(d => d.EmployeeResignationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Resignati__Emplo__1273C1CD");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}

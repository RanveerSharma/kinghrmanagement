﻿using Client.Domain.Repositories;

namespace Client.Repository.Database.HRMANAGEMENT;

public partial class KinghrmanagementContext : IUnitOfWork
{
    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        await SaveChangesAsync(cancellationToken);
        return true;
    }
}
